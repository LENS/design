# Credits

The following people have contributed to LENS.

* @alexbuzzbee: Principal maintainer.
* @flukejones: Rust guru and remover of unnecessary wheel-reinventions.
* @jacobprosser8: Improved build by specifying a nightly compiler.
