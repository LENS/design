# Architecture

LENS's architecture is described in these pages:

* [Filesystem](Architecture/Filesystem.md)
* [S-Tags](Architecture/S-Tags.md)
* [Compartments](Architecture/Compartments.md)
* [Security](Architecture/Security.md)
* [Command-line syntax](Architecture/Command-Line-Syntax.md)
* [Communication](Architecture/Communication.md)
* [Composition](Architecture/Composition.md)
* [Configuration](Architecture/Configuration.md)
* [Standard IO](Architecture/Standard-IO.md)
* [Time](Architecture/Time.md)
* [Help](Architecture/Help.md)
* [Applications](Architecture/Applications.md)
* [Resources](Architecture/Resources.md)
* [Participation](Architecture/Participation.md) (for extensions and automation)
* [Events](Architecture/Events.md) (signals especially)
