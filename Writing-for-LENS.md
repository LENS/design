# Writing for LENS

While it is possible to write "hybrid" LENS programs that use the standard library and the system `libc`, this depends on the presence of a parallel userspace with these components and requires modifications to the LENS `runtime` crate. To write "pure" LENS programs, follow these guidelines:

* Use `#![no_std]` in all crate roots. The standard library has not been ported, so it needs to be disabled.
* Use `#![no_main]` in binary crate roots. Rust's normal startup machinery is (for now) bypassed and those tasks are instead handled in `runtime`.
* Mark your `main` function with the attribute `#[no_mangle]`. Otherwise, the `runtime` startup machinery can't find `main`.
* Use a nightly toolchain. LENS uses a number of `#![feature(...)]` attributes.
* Build using the command `cargo rustc -- -Z pre-link-arg=-nostartfiles` instead of `cargo build`. Otherwise, the linker tries to include the system `crt0`, which conflicts with `runtime`.
* Add the following to `Cargo.toml`. Panic methods more complicated than `abort` want more infrastructure than LENS has available.
```toml
[profile.dev]
panic = "abort"

[profile.release]
panic = "abort"
```
