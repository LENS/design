# LENS design documentation

This is the design documentation (along with a small amount of other information) for LENS, a non-POSIX userspace environment running on the Linux kernel. The original concept for LENS called for a custom kernel, but later consideration led to that idea being shelved.

* [Architecture](Architecture.md) - Architectural design documents for LENS. Mostly unimplemented.
* [Credits](Credits.md) - People who have contributed to the project.
* [Writing for LENS](Writing-for-LENS.md) - Advice on writing programs meant to run on LENS.
