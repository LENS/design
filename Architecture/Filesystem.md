# Filesystem

LENS uses a single-rooted filesystem tree, like Unix, because this is imposed by the kernel and because it is a good design. However, LENS does not use the Linux Filesystem Hierarchy Standard, or any other Unix-like filesystem layout. Below is a basic description of the key locations in the hierarchy:

* `/zone/` - the system default main zone - see [Compartments](Compartments.md).
* `/zone/system.lens-os.xyz/` - the operating system's main compartment.
* `/home/` - user home directories.
* `/home/<qualified username>/zone/` - home directory default main zone - see [Compartments](Compartments.md).
* `/mounts/` - mount points not more appropriate elsewhere.
* `/_/` - pseudo-filesystems.
* `/_/device/` - device special files.
* `/_/kernel/` - Linux `sysfs`.
* `/_/process/` - Linux `proc` filesystem.
* `/_/tmp/` - Linux `tmpfs`; temporary files.

This hierarchy reduces the redundancy and confusing variety of locations in Unix filesystem layouts (e.g. `/bin`, `/usr/bin`, `/usr/local/bin`, `/sbin`, `/usr/sbin`, `/usr/local/sbin`, `/usr/libexec`, `/opt/*/bin`, `~/bin`, `~/opt/*/bin`, and more are all not-uncommon locations for program binaries) and makes it significantly easier to find or know where to place files in the hierarchy.

Instead of the common "filename extensions" model of file type identification, LENS uses Internet Media Types directly. The `user.mime_type` extended attribute stores a file's media type ("MIME type" is a deprecated term for a media type, used by this already-existing extended attribute). If it is missing, the system can try to guess its type based on its contents or, if present, its filename extension. If all else fails, the user can be asked to give the correct type (or the system can fall back to `application/octet-stream`). Since the type is metadata, it is much more difficult to accidentally destroy or change while renaming a file. Files received from the network usually have an associated media type, which is saved by the network application when it writes out the file.
