# Security

LENS implements its userspace security in two main daemons: `authnd`, the authentication daemon; and `authzd`, the authorization daemon. `authnd` is responsible for checking credentials and keeping track of users and groups (together "entities"), and `authzd` is responsible for userspace privilege checks (e.g. if a process wants to create a new user, its owner must be authorized to do that).

`authnd` stores data about entities in abstract key-value lists. These include credentials such as passphrase hashes and public keys; credentials can be flagged to prevent unauthorized reading. Each entity is associated with an "authority," which is a DNS-registered or DNS-like (`.local` or `.invalid`) domain or host name. Different authorities may be provided by different underlying services; for example, `localhost.local` is probably a local database, while `example.org` might be a remote directory server. Each authority has a 15-bit UID/GID prefix, while the remaining 16 bits are assigned by the underlying service.

Certain daemons and programs may perform privileged operations. As such, they have a special Linux capability or (rarely) are `suid`. These daemons and programs have to check authorization before they actually perform the requested operation. They do this by asking `authzd`, which looks at the attributes of the relevant entities, consults a policy, and returns a yes-or-no authorization decision.

Both `authnd` and `authzd` allow [participation](Participation.md) to provide new authorities, authentication schemes, or authorization policies.

Access control lists are used over mode bits for security, as they are much more flexible and make it easier to allow administration without elevating permissions.
