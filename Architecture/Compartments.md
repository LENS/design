# Compartments

LENS manages files belonging to installed software packages using the mechanism of "compartments." A compartment is a directory, named using an [S-Tag](S-Tags.md).

Compartments are stored in directories called "zones." There are two [configuration](Configuration.md) entries specifying zones to search: `system-zones.lens-os.xyz` is a list specifying system-wide zones, and `user-zones.lens-os.xyz` per-user zones. The current user's home directory is substituted for `~` in the latter. This mechanism makes it possible to prevent the user from overriding system-wide settings, by simply ignoring the user zones list.

As configuration is itself stored mostly in compartments, a mechanism is needed to bootstrap the compartment search mechanism. The environment is used for this; the zone containing the core system configuration is listed in an environment entry initialized at boot time in process #1, based on a hard-coded default, auto-detection, or a kernel command-line parameter.

The main effect on the rest of the system of compartments are their *components*. Components are stored in a directory named `components` in the root of a compartment, which contains files and directories named using S-Tags. Each of these is a component of the type given by its name. A component directory can naturally contain many files, and components may be merged together to allow multiple contributors.

The system provides functions for searching for a component. The primary function, `find_components`, finds components or files within components matching a name/type, possibly with wildcards, in any compartment. Convenience functions are available for common component search patterns, such as only retrieving the first result or retrieving all components of a type.

Packages are usually installed with one of the following patterns:

* Immutable. There is only one compartment, in whatever zone is appropriate. Its contents are never modified; the package does not support any configuration or store any data.
* System mutable. There is one system-wide compartment containing immutable files and another containing configuration, data, or customizations.
* User-and-system mutable. As with system mutable, but a per-user compartment is also created for each user who makes customizations or saves per-package data.
* Per-user. As with system mutable, but both compartments are per-user.

All of these patterns enable upgrading a package by merely replacing the immutable compartment, while the mutable patterns allow changes in configuration and data to be moved about, deleted, and generally managed separately from the rest of the package, but still as a unit. Normally, compartments other than the immutable compartments should be created automatically by a package when first needed. Other patterns can be developed and used as needed, e.g. network installation, disk image zones for portable installations, etc.

Compartments can be used for purposes other than packages, such as bundling and organization of data files or to override components provided in packages.
