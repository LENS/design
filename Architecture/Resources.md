# Resources

Unix has the notion that "everything is a file." In LENS, "everything is a resource," a slightly higher-level abstraction. Resources are identified by Internet Uniform Resource Identifiers (URIs, of which URLs are a subset), and come in a variety of types. For example, a file on the local filesystem would be identified by a `file:` URI, e.g. `file:///home/bob@localhost/Documents/Letter`. A file is a "contentful" resource, meaning a stream of bytes can be read from it, and a "mutable" resource, meaning its content can be changed. It is also an "attributed" resource, meaning it has attributes like a size, a last modification time, or a file description (as an extended attribute), a "typed" resource, meaning the Internet media type (e.g. `text/plain`) of its content is known, and an "access-controlled" resource, meaning it has an access-control list that determines who can perform different operations on it.

LENS represents these different aspects of a file as Rust traits. A regular file would have at least these traits:

| Trait | Function |
|-|-|
| `SeqentialReadContent` | Read bytes from the resource's content in order. |
| `RandomReadContent` | Read bytes from the resource's content out of order, at specific locations. |
| `AppendContent` | Add to the content of the resource, at the end. |
| `WriteContent` | Change the content of the resource. |
| `GetAttribute` | Retrieve attributes by name. |
| `SetAttribute` | Set attributes by name. |
| `GetType` | Get the media type of the resource. |
| `CheckPermission` | Check if access control allows an operation. |
| `GetPermission` | Read the access control list of the resource. |
| `SetPermission` | Modify the access control list of the resource. |
| `Move` | Move the resource to a different hierarchical parent. |
| `Copy` | Copy the resource, creating a similar resource with a different hierarchical parent. |


Typically, these traits are bundled into larger traits (e.g. `FileLikeResource`) for convenience. Some files, like device files, might not have all of these traits (e.g. a terminal can only be read sequentially and appended to; it can't be accessed out of order). Other files, like directories, would have traits regular files don't; directories have children, so they would have these traits:

| Trait | Function |
|-|-|
| `GetChildren` | Get URIs of hierarchical children of the resource. |
| `ChangeChildren` | Create or remove hierarchical children of the resource. |

When an attempt is made to obtain a trait object for a trait that doesn't apply, an error occurs, preventing a program from doing impossible things like getting the children of a regular file or the content bytes of a directory.

There are, of course, resources other than local files. To name a few: Standard input, output, and log, and other inherited handles, can be accessed by the `lens-handle:` scheme; Internet resources, such as Web sites, email inboxes, or FTP servers can be accessed by their own schemes, as first-class citizens; and [components](Compartments.md) can be accessed by the `lens-component:` scheme, automatically invoking the component resolution mechanism.

Since the system libraries cannot include every possible protocol, separate helper programs are used to access many types of resources. These programs run as demand-started per-session daemons, allowing them to keep caches, authentication tokens, and session identifiers without causing a security problem, and letting them shut down automatically to conserve resources when not in use.
