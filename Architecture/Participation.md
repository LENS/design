# Participation

General-purpose extension points, which are also used for automation purposes, are provided by "participation." To allow participation, applications or components create a [messenger server](Communication.md) for each instance run. Each participant connects to this server, allowing it to send and receive "questions," "answers," "statements," and "commands." All of these messages are relayed to all other participants, including the server.

* A question requests answers from other participants, such as to provide the opportunity to change parameters of an action in progress, or to determine what features are available from different extensions.
* A command instructs a particular participant to perform an action. The availability of commands should be determined through questions before issuing them.
* An answer responds to a question or command. All participants give at least an empty answer to each question, except their own. Only commanded participants respond to commands.
* A statement provides an announcement that an event has occurred, such as when participants connect and disconnect, or of some fact or value.

To ease support for scripting languages, the values carried by these messages must consist of a map of strings to `u64`s, `f64`s, strings, byte slices, vectors of allowed types, and maps of strings to allowed types. Each is identified by a message name, indicating its format, and a message ID, which incorporates the originating process ID and a unique number. Commands also have a name for the target, and answers have the message ID of the message replied to.

Participation groups can be created using a dedicated participation server daemon in the case of [composition apparatuses](Composition.md) or when the nominal main participant wants to run the participation group out-of-process.
