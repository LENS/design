# Standard IO

Standard IO on Unix takes the form of three file descriptors: `stdin` (0), `stdout` (1), and `stderr` (2). These standard streams can refer to any file, but they only allow input and output of byte sequences, which makes working with structured data awkward.

LENS standard IO resolves this problem by using object flows.

Standard output accepts objects that are both `Serialize` and `Display`. The exposed standard output flow is actually a filter that wraps the "raw" flow created from handle 1. If the raw flow is a terminal, objects are formatted and written with a trailing newline. If the raw flow is another kind of byte flow, objects are serialized and written with a leading length. If the raw flow is a packet flow, objects are serialized and sent to the flow. These conversions are done using stock filters from `lens_flows` and `postcardflows`, which are stacked on the raw flow when standard output is initialized.

Standard input can operate in either of two modes. In "line" mode, standard input reads a line of text at a time and returns `&str`s. In "object" mode, standard input uses similar filters to standard output on non-terminals.

Access to the raw input and output flows is available when necessary, but programs must be prepared to deal with the wide variety of possible raw flows.

"Standard log" (the equivalent of `stderr`) is more structured and rigid than standard input and output. It is used for general-purpose logging; objects written to the standard error flow are instances of a `struct` called `Message`. There are several levels of message, distinguished by an `enum`: Debug, Verbose, Info, Success, Warning, Error, Critical. Each message has a source (identifying the package, program, crate, module, and function), a property map, a sequence number, a time, and a human-readable message string. These message records are processed similar to objects sent to standard output. They may be delivered via a messenger, an event topic, or other means to a log-handling process that saves them to a log file, displays them in a graphical environment, sends them to an aggregator, analyzes them, or anything else that might be necessary. Log processors usually write the messages they process to standard output, allowing them to be chained.

Access to the raw standard log flow is not available, to prevent sending invalid messages.

The structure of standard log enforces a clear separation of purpose between it and standard output: Standard output is for data, and standard log is for log messages. While this distinction exists on Unix, it is not enforced, because both `stdout` and `stderr` are simply byte streams. This often leads to log messages being written to standard output, mixed with other output data, and confusing another process down the line.
