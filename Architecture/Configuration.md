# Configuration

LENS provides a unified system for configuration, with all configuration sources being combined into a single set of layered configuration values. The following sources are included, in order:

* Hard-coded defaults.
* Configuration components (files).
* Environment block (session configuration).
* Command-line arguments (process configuration).

The normal component search process is used for configuration components, so for example user zones, which are searched after system zones, will override the system-level configuration.

Configuration uses S-Tags as keys, for example `weight.font.quicknote.example.com`. Command-line parsing requires specifying a default suffix S-Tag to apply to keys without dots; keys specified on the command line that contain a dot are absolute instead of using this suffix.

Configuration values can be strings or lists. Strings can only be set, but lists can also be appended or prepended to. The zone path, for example, is a list, so configuration entries can add new values without having to know all other zones used.

The configuration file syntax looks like this:

```
weight.font.quicknote.example.com = 12
family.font.quicknote.example.com = Times New Roman
servers.quicknote.example.com := sync.example.com;quicknote.local
directories.quicknote.example.com += ~/Notes
```

Environment block entries are similar, but omit the spaces and use separate entries instead of lines. Unix would interpret LENS environment entries as variables, with list initializiations having names ending with `:` and additions `+`.

To avoid loading every configuration file on the system into every process, each configuration component file has a namespace. Namespaces are generally the suffix of the S-Tag keys the component includes. A given instance of the configuration system only loads components for the namespaces requested.

Additional configuration sources can be provided by libraries if needed.
