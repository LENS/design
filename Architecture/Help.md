# Help

On-line help in LENS takes multiple forms, of different levels of accessibility.

Shell commands and other programs taking command-line arguments use the LENS system argument parser, which allows them to specify "friendly" names and both summary and detail help text for the program, and for each argument. When the `usage=+` or `help=+` argument is specified, the argument parser outputs this help information in the form of a `CommandUsage` object on standard output. This would normally be formatted for display on a terminal, but could also be interpreted by another program and used, e.g., to provide a graphical dialog box for invoking the command.

Each package may provide its own pages for the LENS manual, which is divided into sections for different purposes. The manual provides detailed information, including discussion of key concepts, designs, and internals, how-to guides for common tasks, and solutions to common problems. The manual has a table of contents for each section, as well as an index and glossaries. Hyperlinks, both internal and external, are used heavily. Different programs may be used to access the manual from different contexts, such as separate terminal and GUI readers. Sections or pages of the manual, or the entire collection, can be copied in various formats to other machines, or printed, for reference in case of serious problems.

The command `help` opens a manual reader to the introduction section to explain command-line use of LENS.

Rust API documentation produced by the Rust toolchain is also included with the system as a programming reference.
