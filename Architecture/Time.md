# Time

LENS handles time in a way that appears on the surface to be very complicated. This surface complexity allows the internals to be much simpler in likely future scenarios.

LENS' basic time primitive is the "local linear timestamp." The local linear timestamp, or LLT, is a number that increases by one per second. It ignores leap seconds, daylight savings time, time zones, and all other planetary or political factors. However, LLTs are *local*; they only apply to a particular "time context," and must be converted to other contexts. Fortunately, LLT contexts are very large or very isolated, so these conversions are rare. This locality exists due to relativistic time dilation factors, which are a problem for deep-space or interplanetary timekeeping. LENS considers these applications important to explicitly prepare for in advance.

Unlike a monotonic clock (which does not have any particular relationship to the rest of the world's clocks), every LLT corresponds to a single instant in time, and the same instant on every machine within a context; greater LLTs of the same context are always later in time. Because this correspondence is linear, simple arithmetic is sufficient to perform many time calculations (though not those that must respect the calendar). Conversion to planetary calendars is, as a result of this internal simplicity, more complicated.

In fact, LENS does not inherently understand any time units or formats other than the SI second and the LLT. Each calendar is specified in a set of data files which include unit definitions, date and time formats, time zones, leap seconds, time offsetting rules such as daylight savings time, the epoch, and so on.

Date and time conversions occur at the "edge;" received dates and times are converted into LLTs, and all calculation and other timekeeping work is done on the LLTs. When necessary for interoperation or human readability, the LLTs are converted according to the calendar definition.
