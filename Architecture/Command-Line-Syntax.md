# Command-line syntax

LENS' command-line syntax is part of its [configuration system](Configuration.md). Command-line parameters set configuration values.

There are two types of arguments: Configuration entries and positional arguments.

Configuration entries are in the configuration entry syntax, but whitespace-separated (with backslash, double-quote, or single-quote escaping) instead of line-separated, and with the additional feature that entries whose names lack a `.` automatically have a program-specified suffix appended. Options are represented with the values `+` (to enable) and `-` (to disable). Additionally, the syntactic sugar `+x` allows a named argument to be given as `+` or `-` via a one-character alias. Multiple arguments may be specified like this in a single group, for example: `-fa` would disable the options with the aliases `f` and `a`.

The first argument that is not a configuration entry (option groups included) switches parsing from configuration entries to positional arguments. If the first positional argument needs to start with a `+` or a `-` or contain an `=`, it can be preceded by the marker `-=` (or `config-entries.argparse.lens-os.xyz=-`), which explicitly marks the following arguments as positional. Each program specifies how to handle its positional arguments. Positions may be assigned configuration entries they correspond to, or may be accumulated into a list entry.

Additionally, LENS has a standard for naming of commands. Names are hierarchical, with a general topic (often a noun) at the top level, and more specific elements (often verbs) following. Very common commands may have one-word aliases. For example:

Command name | Function
-------------|---------
`res.copy`, `copy`| Copy one or more resources.
`app.launch`, `launch`| Run an application.
`pkg.add`    | Add (install) a software package.
`daemon.start`| Start a daemon program.
`raid.rebuild.start`| Initiate a RAID array rebuild operation (contrast `raid.rebuild.cancel`, `raid.new`).
`fs.new.vfat`| Create a VFAT filesystem.

This style provides greater freedom to merge and split subcommands into different programs as required.
