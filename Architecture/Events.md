# Events

LENS provides an event library for handling asynchronous events, including, among others, resources becoming ready, incoming messages, and (especially) signals.

The event system is based on the `epoll` mechanism, and is therefore primarily focused on kernel objects. This includes objects like messengers, signal sets (via `signalfd`), and (via signals) timers, but not all possible events. Some types of in-process event can be covered using a self-messenger or self-pipe, but the system event library is not a complete asynchronous event loop mechanism.

Objects able to generate events implement a `Wait` trait, and can be added to an event group along with a closure. The event group can be called to await (possibly with timeout) and then invoke the closure for the next event, returning information about what that event was. Objects can be set to a "just once" mode, in which they are automatically removed once they have generated an event.

Urgent events, which are always implemented with signals, can be handled by *interventions*. An intervention runs an unsafe closure on an alternate stack as soon as the event in question happens, suspending the code that was running prior to the event. Because an intervention can happen at any point, the states of most data structures are potentially unstable. They therefore can only use not merely atomic or lockless but totally waitless data structures (as any wait would be a deadlock), or need to handle the possibility that the data they access will be in an unstable state. Interventions are therefore not recommended for use in normal code.

Similar to Unix, signals can also be handled by ignoring them, deferring them, or terminating the program in several ways: Exiting with a kernel-defined exit code, the same plus dumping core, exiting with a program-defined exit code (using a system-provided intervention closure), or panicking (also using a system-provided intervention closure, performing the panic by editing the signal frame to return to the panic machinery with appropriate parameters).

As with Unix, some signals can only be handled in kernel-defined ways. `Signal::ForceFreeze` (aka `SIGSTOP`) always freezes the process, `Signal::Resume` (aka `SIGCONT`) always unfreezes it (though it can also be reacted to by intervention or event), and `Signal::ForceTerminate` (aka `SIGKILL`) always exits with a kernel-defined exit code.
