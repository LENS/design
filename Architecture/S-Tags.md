# S-Tags

A LENS S-Tag, or System Tag, is a text string used to uniquely identify an entity on a LENS system. Incompatible versions of an entity should be assigned separate S-Tags. S-Tags are based on domain names. While reverse-order DNS is frequently used, LENS uses the forward order so that the most specific (and thus usually most relevant) part of the tag comes first. A domain name controlled by the S-Tag issuer is used as a suffix, and a hierarchy of tags below that name can be constructed to name different entities, for example:

* `example.com` - The Example Corporation, which publishes LENS software.
  * `quicknote.example.com` - The main package of QuickNote, a note-taking application for LENS by Example Corp.
    * `preferences.quicknote.example.com` - The configuration component storing the user's preferences for QuickNote.
    * `app.quicknote.example.com` - The main QuickNote application.
      * `program.app.quicknote.example.com` - The program run for the main QuickNote application.

A service will be provided to allow registering names somewhere under the LENS domain for users who want to issue S-Tags but do not own a domain name.
